
# Results obtained using Kendall's rank correlation coefficient

The same numerical experiment described in the paper have been repeated using
Kendall's rank correlation coefficient instead of Spearman correlation
coefficient.

## Results

The scatter plots obtained with $`n={3,4,5,6,7,8}`$ are depicted below.

### Universal set with $`n=3`$ elements

![The scatter plots obtained by means of simulation with n=3.](kendall-scatter_heat3.png)

### Universal set with $`n=4`$ elements

![The scatter plots obtained by means of simulation with n=4.](kendall-scatter_heat4.png)

### Universal set with $`n=5`$ elements

![The scatter plots obtained by means of simulation with n=5.](kendall-scatter_heat5.png)

### Universal set with $`n=6`$ elements

![The scatter plots obtained by means of simulation with n=6.](kendall-scatter_heat6.png)

### Universal set with $`n=7`$ elements

![The scatter plots obtained by means of simulation with n=7.](kendall-scatter_heat7.png)

### Universal set with $`n=8`$ elements

![The scatter plots obtained by means of simulation with n=8.](kendall-scatter_heat8.png)
