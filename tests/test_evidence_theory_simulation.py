
import timeit
import unittest
import numpy as np
from evidence_theory_simulation import __version__
from evidence_theory_simulation import core
from evidence_theory_simulation import experiment


class TestCore(unittest.TestCase):

    def setUp(self):
        # declare data
        powerset = core.powerset(4)
        mass = np.zeros(powerset.shape[0])
        # BOE 1
        data = [
            {
                "element": np.array([1.0, 1.0, 0.0, 0.0]),
                "mass": 1/3
            },
            {
                "element": np.array([0.0, 1.0, 1.0, 0.0]),
                "mass": 2/3
            }
        ]
        # BOE 2
        data = [
            {
                "element": np.array([0.0, 0.0, 0.0, 1.0]),
                "mass": 0.05
            },
            {
                "element": np.array([0.0, 1.0, 0.0, 1.0]),
                "mass": 0.15
            },
            {
                "element": np.array([1.0, 1.0, 0.0, 0.0]),
                "mass": 0.2
            }
        ]
        for i in range(len(mass)):
            for j in range(len(data)):
                if not np.any(np.subtract(powerset[i], data[j]["element"])):
                    mass[i] = data[j]["mass"]

        self.data = core.generate_dataset(powerset, mass)
        self.precision = 4

    def test_version(self):
        self.assertEqual(__version__, '0.1.0')

    def test_powerset(self):
        m = 3
        ps = core.powerset(m)
        self.assertEqual(ps.shape[0], 2**m)

    def test__sample_mass_values(self):
        n = 4
        w = core._sample_mass_values(n)
        self.assertAlmostEqual(np.sum(w), 1)

    def test_sample_mass(self):
        n = 4
        w = core.sample_mass(core.powerset(n))
        self.assertAlmostEqual(np.sum(w), 1)

    def test_belief_plausibility(self):
        m = 4
        powerset = core.powerset(m)
        mass = core.sample_mass(powerset)
        belief = core.mass2belief(powerset, mass)
        plausibility = core.mass2plausibility(powerset, mass)
        self.assertTrue(np.all(belief <= plausibility))

    def test_hohle(self):
        ho = core.hohle(self.data)
        self.assertAlmostEqual(ho, 0.918296, self.precision)

    def test_smets(self):
        hs = core.smets(self.data)
        self.assertAlmostEqual(hs, 0.918296, self.precision)

    def test_yager(self):
        hy = core.yager(self.data)
        self.assertAlmostEqual(hy, 0.0, self.precision)

    def test_nguyen(self):
        hn = core.nguyen(self.data)
        self.assertAlmostEqual(hn, 0.918296, self.precision)

    def test_dubois_prade(self):
        hdp = core.dubois_prade(self.data)
        self.assertAlmostEqual(hdp, 1.0, self.precision)

    def test_lamata_moral(self):
        hlm = core.lamata_moral(self.data)
        self.assertAlmostEqual(hlm, 1.0, self.precision)

    def test_klir_parviz(self):
        hkp = core.klir_and_parviz(self.data)
        self.assertAlmostEqual(hkp, 0.370344, self.precision)

    def test_pal_et_al(self):
        hp = core.pal_et_al(self.data)
        self.assertAlmostEqual(hp, 1.9183, self.precision)

    def test_deng(self):
        hd = core.deng(self.data)
        self.assertAlmostEqual(hd, 2.50326, self.precision)

    def test_jirousek_and_shenoy(self):
        hjs = core.jirousek_and_shenoy(self.data)
        self.assertAlmostEqual(hjs, 2.45915, self.precision)

    def test_quin_et_al(self):
        hq = core.quin_et_al(self.data)
        self.assertAlmostEqual(hq, 1.4183, self.precision)

    def test_yan_and_deng(self):
        hyd = core.yan_and_deng(self.data)
        self.assertAlmostEqual(hyd, 2.02236, self.precision)

    def test_li_et_al(self):
        hl = core.li_et_al(self.data)
        self.assertAlmostEqual(hl, 17/10)

    def test_li_and_pan(self):
        hlp = core.li_and_pan(self.data)
        self.assertAlmostEqual(hlp, 4.9183, self.precision)

    def test_jousselme_et_al(self):
        hj = core.jousselme_et_al(self.data)
        self.assertAlmostEqual(hj, 1.45915, self.precision)

    def test_fractal_based_entropy(self):
        hfb = core.fractal_based_entropy(self.data)
        self.assertAlmostEqual(hfb, 2.19716, self.precision)

    def test_george_and_pal(self):
        hgp = core.george_and_pal(self.data)
        self.assertAlmostEqual(hgp, 8/27, self.precision)

    def test_wang_and_song(self):
        hws = core.wang_and_song(self.data)
        self.assertAlmostEqual(hws, 2.45915, self.precision)

    def test_deng_and_wang(self):
        hdw = core.deng_and_wang(self.data)
        self.assertAlmostEqual(hdw, 2.39385, self.precision)
