
import os
import sys
sys.path.append('..')
import click
import traceback
from evidence_theory_simulation import core, experiment


@click.command()
@click.option('-m', default=4, help='The number of elements in X.')
@click.option('-n', help='A name for the experiment.')
def main(m, n):
    N = 100  # number of samples
    f_name = f"notebooks/results{m}-{n}.csv"
    entropy_measures = [
        core.hohle,
        core.smets,
        core.yager,
        core.nguyen,
        core.dubois_prade,
        core.lamata_moral,
        core.klir_and_ramer,
        core.klir_and_parviz,
        core.pal_et_al,
        core.deng,
        core.jirousek_and_shenoy,
        core.qin_et_al,
        core.yan_and_deng,
        core.li_et_al,
        core.harmanec_and_klir,
        core.li_and_pan,
        core.pan_and_deng,
        core.deng_and_wang,
        core.jousselme_et_al,
        core.yang_and_han,
        core.fractal_based_entropy,
        core.cui_et_al,
        core.george_and_pal,
        core.wang_and_song,
        core.zhou_et_al
    ]
    if f_name in os.listdir():
        r = pd.read_csv(f_name, index_col=0)
    else:
        try:
            r = experiment.experiment(N, m, entropy_measures)
            r.to_csv(f_name)
        except Exception as e:
            print(traceback.format_exception(e))


if __name__ == '__main__':
    main()
