
import numpy as np
import pandas as pd
from copy import deepcopy


def namer(name):
    def wrapper(f):
        f.name = name
        return f
    return wrapper


def period(period):
    assert type(period) is int, f"'period' must be int and is {type(period)}."
    assert 1 <= period <= 4
    def wrapper(f):
        f.period = period
        return f
    return wrapper


def classification(classification):
    def wrapper(f):
        f.classification = classification
        return f
    return wrapper


def powerset(m):
    """
    Generates the power set of a frame of descerment with `m` elements.

    Parameters
    ----------
    m: (int)
        The number of elements in the frame of descernment `X`.

    Returns
    -------
    powerset: (numpy array)
        A boolean array with size :math:`2^m \\times m` that defines whether
        an element of `X` (on columns) belongs to the element (on rows) of the
        power set, and `m` is number of elements in `X`.
    """
    powerset = [np.zeros((m,))]
    for i in range(m):
        for j in range(len(powerset)):
            x = np.copy(powerset[j])
            x[i] = 1
            powerset.append(x)
    return np.stack(powerset)


def sample_mass(powerset):
    """
    Sample mass values for some elements of the powerset so that all the
    elements of the frame of descernment are covered by mass assignment.

    Parameters
    ----------
    powerset: (numpy array)
        A boolean array with size :math:`2^m \\times m` that defines whether
        an element of `X` (on columns) belongs to the element (on rows) of the
        power set, and `m` is number of elements in `X`.
    """
    # remember to exclude the empty set from mass assignment
    idxs = list(range(1, powerset.shape[0]))
    # sample a random element and add it to the list of mass assignements
    mass_assign = [idxs.pop(np.random.randint(len(idxs)))]
    x = powerset[mass_assign[0]].copy()
    if np.all(x):
        stop = True
    else:
        stop = False

    while not stop:
        mass_assign.append(idxs.pop(np.random.randint(len(idxs))))
        # compute the stopping condition
        x += powerset[mass_assign[-1]]
        if np.all(x):
            stop = True
    w = np.random.dirichlet(np.ones(len(mass_assign)))
    mass = np.zeros(powerset.shape[0])
    for i, idx in enumerate(mass_assign):
        mass[idx] = w[i]
    return mass


def is_subset(a, b):
    """
    Returns True if `a` is a subset of `b`.
    """
    return np.array_equal(np.multiply(a, b), a)


def mass2belief(powerset, mass):
    """
    Compute the belief values from a mass values.
    """
    belief = np.zeros_like(mass)
    for i, e1 in enumerate(powerset):
        m = np.zeros_like(mass)
        for j, e2 in enumerate(powerset):
            if np.array_equal(np.multiply(e1, e2), e2):
                m[j] = mass[j]
        belief[i] = np.sum(m)
    return belief


def mass2plausibility(powerset, mass):
    """
    Compute the plausibility values from a mass values.
    """
    plausibility = np.zeros_like(mass)
    for i, e1 in enumerate(powerset):
        m = np.zeros_like(mass)
        for j, e2 in enumerate(powerset):
            if np.any(np.multiply(e1, e2)):
                m[j] = mass[j]
        plausibility[i] = np.sum(m)
    return plausibility


def cardinalities(powerset):
    """
    Compute the cardinality of each set of the powerset.
    """
    assert len(powerset.shape) == 2
    return np.sum(powerset, axis=1)


def commonality(powerset, mass):
    """
    Compute the commonality measure.
    """
    commonality = np.zeros_like(mass)
    for i, a in enumerate(powerset):
        m = np.zeros_like(mass)
        for j, b in enumerate(powerset):
            if is_subset(a, b):
                m[j] = mass[j]
        commonality[i] = np.sum(m)
    return commonality


def generate_dataset(powerset, mass=None):
    """
    Generate a list of dictionaries ready to be transformed to a Pandas
    DataFrame for visualization.
    A probability mass value is assigned to `known_mass_elements` elements of
    the power set and all the values of the mass sum to 1.

    Parameters
    ----------
    powerset: (numpy array)
        An boolean array with size :math:`m \\times 2^m` that define whether an
        element of `X` (on columns) belongs to the element (on rows) of the
        power set.

    mass: (numpy array)
        The mass values for each element of the powerset.

    Returns
    -------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing mass, belief, and plausibility values
        for the elements of the set.
    """
    if mass is None:
        mass = sample_mass(powerset)
    beliefe = mass2belief(powerset, mass)
    plausibility = mass2plausibility(powerset, mass)
    comm = commonality(powerset, mass)
    card = cardinalities(powerset)
    data = {
        "element": [i for i in powerset],
        "mass": mass,
        "belief": beliefe,
        "plausibility": plausibility,
        "commonality": comm,
        "card": card
    }
    df = pd.DataFrame(data)
    return df


@namer("Höhle")
@period(1)
@classification('D, EB')
def hohle(data):
    """
    Computes the Hohle entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.

    """
    data = data[data.belief > 0]
    return np.sum(data.mass * np.log2(1 / data.belief))


@namer("Smets")
@period(1)
@classification('D, EB')
def smets(data):
    """
    Computes the Smets entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    return np.sum(np.log2(1 / data.commonality))


@namer("Yager")
@period(1)
@classification('D, EB')
def yager(data):
    """
    Computes the Yager entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.plausibility > 0]
    return np.sum(data.mass * np.log2(1 / data.plausibility))


@namer("Nguyen")
@period(1)
@classification('D, EB')
def nguyen(data):
    """
    Computes the Nguyen entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    return np.sum(data.mass * np.log2(1 / data.mass))


@namer("Dubois & Prade")
@period(1)
@classification('NS, EB')
def dubois_prade(data):
    """
    Computes the Dubois and Prade entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    return - np.sum(data.mass * np.log2(1 / data.card))


@namer("Lamata & Moral")
@period(2)
@classification('TU, EB')
def lamata_moral(data):
    """
    Computes the Lamata and Moral entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    return yager(data) + dubois_prade(data)


@namer("Klir & Ramer")
@period(2)
@classification('TU, EB')
def klir_and_ramer(data):
    """
    Computes the Klir and Ramer entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    kr = 0.0
    for i in data.index:
        x = 0.0
        for j in data.index:
            x += data.loc[j].mass * np.sum(data.loc[i].element * \
                data.loc[j].element) / data.loc[j].card
        kr -= data.loc[i].mass * np.log2(x)
    return kr


@namer("Klir & Parviz")
@period(2)
@classification('TU, EB')
def klir_and_parviz(data):
    """
    Computes the Klir and Parviz entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    kp = 0.0
    for i in data.index:
        x = 0.0
        for j in data.index:
            x += data.loc[j].mass * np.sum(data.loc[i].element * \
                data.loc[j].element) / data.loc[i].card
        kp -= data.loc[i].mass * np.log2(x)
    return kp


@namer("Pal et al.")
@period(2)
@classification('TU, EB')
def pal_et_al(data):
    """
    Computes the Pal et al. entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    return np.sum(data.mass * np.log2(data.card / data.mass))


@namer("Deng")
@period(4)
@classification('TU, EB')
def deng(data):
    """
    Computes the Deng entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    return nguyen(data) + np.sum(data.mass * np.log2(np.power(2, data.card) - 1))


@namer("Jiroušek & Shenoy")
@period(4)
@classification('TU, EB')
def jirousek_and_shenoy(data):
    """
    Computes the Jirousek and Shenoy entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    K = 0.0
    pl_pm = []
    for i in data.index:
        if np.sum(data.loc[i].element) == 1:
            K += data.loc[i].plausibility
            pl_pm.append(data.loc[i].plausibility)
    pl_pm = [i for i in pl_pm if i != 0]
    pl_pm = np.array(pl_pm) / K
    return - np.dot(pl_pm, np.log2(pl_pm)) + dubois_prade(data)


@namer("Qin et al.")
@period(4)
@classification('TU, EB')
def qin_et_al(data):
    """
    Computes the Quin et al. entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    return np.sum((data.card / len(data.iloc[0].element)) * data.mass * \
        np.log2(data.card)) + nguyen(data)


@namer("Yan & Deng")
@period(4)
@classification('TU, EB')
def yan_and_deng(data):
    """
    Computes the Yan and Deng entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    S = 0
    for i in data.index:
        if np.sum(data.loc[i].element) == 1 and data.loc[i].plausibility > 0:
            S += 1
    data = data[data.mass > 0]
    return - np.sum(
        data.mass * np.log2(
            np.multiply(
                (data.mass + data.belief) / \
                    (2 * (np.power(2, data.card) - 1)),
                np.exp((data.card - 1) / S)
            )
        )
    )


@namer("Li et al.")
@period(4)
@classification('TU, IB')
def li_et_al(data):
    """
    Computes the Li et al. entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    li = 0.0
    for i in data.index:
        if np.sum(data.loc[i].element) == 1:  # the element is a singleton
            # compute the distance with the interval [0, 1]
            d_E = np.sqrt(
                data.loc[i].belief**2 + (data.loc[i].plausibility - 1)**2
            )
            li += (2 / (1 + d_E)) - 1
    return li


@namer("Harmanec & Klir")
@period(2)
@classification('TU, EB')
def harmanec_and_klir(data):
    """
    Computes the Aggregated Uncertainty using Van-Nam and Yoshiteru algorithm.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    m = len(data.iloc[0]["element"])
    data = data[data.mass > 0]
    AU = 0.0
    data = data.to_dict('records')
    while len(data) > 1:
        # find the powerset of F
        pset = powerset(len(data))
        # compute U(F)
        UF = []
        for i in pset:
            if np.sum(i) >= 1:  # do not consider the empty set
                x = np.zeros((len(data[0]["element"]), ))
                belief = 0.0
                for j in np.nonzero(i)[0]:
                    x = np.maximum(x, data[j]["element"])
                    belief += data[j]["mass"]
                UF.append({
                    "element": x,
                    "belief": belief,
                    "card": np.sum(x)
                })
        # select the element with the highest Bel(A) / |A|
        idx = np.argmax([i["belief"] / i["card"] for i in UF])
        # if there is more than one element with the same Bel(A) / |A|, select
        # the one with the highest cardinality
        if type(idx) is np.array and len(idx) > 1:
            idx = idx[np.argmax([UF[i]["card"] for i in idx])]
        A = UF[idx]
        for i in np.nonzero(A["element"])[0]:
            p_x = A["belief"] / np.sum(A["element"])
            AU -= (A["belief"] * np.log2(p_x))
        # find F'
        F = [{
                "element": np.maximum(
                    np.subtract(A_i["element"], A["element"]),
                    np.zeros_like(A_i["element"])
                )
            } for A_i in data
        ]
        # remove the empty set from F
        d = []
        for i in F:
            if np.any(i["element"]):
                d.append(i)
        F = d
        # compute the mass of each S_i
        d = []
        for S in F:
            S["mass"] = np.sum([
                A_i["mass"] for A_i in data if not np.any(
                    np.subtract(
                        S["element"],
                        np.maximum(
                            np.subtract(A_i["element"], A["element"]),
                            np.zeros_like(A_i["element"])
                        )
                    ))])
            S["card"] = np.sum(S["element"])
        data = deepcopy(F)
    # compute the last contribution to AU
    if len(data) == 1:
        AU -= data[0]["mass"] * np.log2(data[0]["mass"] / data[0]["card"])
    return (1 / m) * AU


@namer("Li & Pan")
@period(4)
@classification('TU, EB')
def li_and_pan(data):
    """
    Computes the Li and Pan entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    return np.sum(
        np.multiply(
            data.mass,
            np.log2(data.card**len(data.iloc[0].element) / data.mass)
        )
    )


@namer("Zhou & Tang")
def zhou_and_tang(data):
    """
    Computes the Zhou and Tang entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    log_arg = np.multiply(
        data.mass / 2**data.card - 1,
        np.exp((data.card - 1) / len(data.iloc[0].element))
    )
    return - np.sum(data.mass * np.log2(log_arg))


@namer("Pan & Deng")
@period(4)
@classification('TU, EB')
def pan_and_deng(data):
    """
    Computes the Pan and Deng entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    return - np.sum(
        np.multiply(
            (data.belief + data.plausibility) / 2,
            np.log2(
                (data.belief + data.plausibility) / (2 * (2**data.card - 1))
            )
        )
    )


@namer("Deng & Wang")
@period(4)
@classification('TU, IB')
def deng_and_wang(data):
    """
    Computes the Deng and Wang entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    du = 0.0
    for i in data.index:
        if np.sum(data.loc[i].element) == 1:
            du += 1 - np.sqrt(data.loc[i].belief + data.loc[i].plausibility + \
                1 - 2 * np.sqrt(data.loc[i].plausibility))
    return du


@namer("Jousselme et al.")
@period(3)
@classification('TU, EB')
def jousselme_et_al(data):
    """
    Computes the Jousselme et al. entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    am = 0.0
    for i in data.index:
        if np.sum(data.loc[i].element) == 1:
            bet = 0.0
            for j in data.index:
                if is_subset(data.loc[i].element, data.loc[j].element):
                    bet += data.loc[j].mass / data.loc[j].card
            if bet != 0.0:
                am += bet * np.log2(bet)
    return - am


@namer("Yang & Han")
@period(4)
@classification('TU, IB')
def yang_and_han(data):
    """
    Computes the Yan and Hang entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    tu = 0.0
    for i in data.index:
        x = data.loc[i]
        if np.sum(x.element) == 1:
            tu += np.sqrt(
                ((x.belief + x.plausibility) / 2 - 1 / 2)**2 + \
                    1 / 3 * ((x.plausibility - x.belief) / 2 - 1 / 2)**2
            )
    return 1 - (1 / len(data.iloc[0].element)) * np.sqrt(3) * tu


@namer("Zhou & Deng")
@period(4)
@classification('TU, EB')
def fractal_based_entropy(data):
    """
    Computes the fractal based entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.card > 0]
    m_F = np.zeros_like(data.mass)
    for i in data.index:
        for j in data.index:
            if is_subset(data.loc[i].element, data.loc[j].element):
                m_F[i - 1] += data.loc[j].mass / (2**data.loc[j].card - 1)
    # filter out elements with mass equal to zero
    data = data[m_F > 0]
    m_F = m_F[m_F > 0]
    return - np.sum(m_F * np.log2(m_F))


@namer("Cui et al.")
@period(4)
@classification('TU, EB')
def cui_et_al(data):
    """
    Computes the Cui et. al. entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    card_X = len(data.iloc[0].element)
    cui = 0.0
    for i in data.index:
        int_sum = 0.0
        for j in data.index:
            if not np.array_equal(data.loc[i].element, data.loc[j].element):
                int_sum += np.sum(data.loc[i].element * data.loc[j].element) \
                    / (2**card_X - 1)
        cui += data.loc[i].mass * np.log2(
            (data.loc[i].mass / (2**data.loc[i].card - 1)) * np.exp(int_sum)
        )
    return - cui


@namer("George & Pal")
@period(2)
@classification('TU')
def george_and_pal(data):
    """
    Computes the George and Pal entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    gp = 0.0
    for i in data.index:
        a = data.loc[i]
        int_sum = 0.0
        for j in data.index:
            b = data.loc[j]
            int_sum += b.mass * (1 - (np.sum(a.element * b.element) / np.sum((a.element + b.element) > 0)))
        gp += a.mass * int_sum
    return 1 - gp


@namer("Wang & Song")
@period(4)
@classification('TU, IB')
def wang_and_song(data):
    """
    Computes the Wang and Sung entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    ws = 0.0
    for i in data.index:
        a = data.loc[i]
        if np.sum(a.element) == 1 and a.belief + a.plausibility != 0:
            ws += - ((a.belief + a.plausibility) / 2) * \
                np.log2((a.belief + a.plausibility) / 2) + \
                    (a.plausibility - a.belief) / 2
    return ws


@namer("Zhou et al.")
@period(4)
@classification('TU, EB')
def zhou_et_al(data):
    """
    Computes the Zhou et al. entropy of the powerset.

    Parameters
    ----------
    data: (pandas DataFrame)
        A `pandas.DataFrame` containing the mass, belief, and plausibility
        values of the elements of a powerset.
    """
    data = data[data.mass > 0]
    card_X = len(data.iloc[0].element)
    return - np.sum(np.multiply(
        data.mass,
        np.log2(
            data.mass / (2**data.card - 1)) * np.exp((data.card - 1) / card_X
        )
    ))


if __name__ == "__main__":
    pass
