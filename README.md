
# A comparative study of uncertainty measures in the Dempster-Shafer evidence theory

The repository contains supplementary material to support the paper *"A comparative study of uncertainty measures in the Dempster-Shafer evidence theory"*.

## Get started

The packages required to run the experiment are listed in `requirements.txt`.
We suggest to create a Python >= 3.9 virtual environment and to install the required packages

```bash
    $ python -m venv <virtual_environment_name>
    $ source <virtual_environment_name>/bin/activate
    $ pip install -r requirements.txt
```

The procedure is valid for Linux users. Windows users can run the following commands in the commad prompt

```cmd
    > python -m venv <virtual_environment_name>
    > <virtual_environment_name>/Scripts/activate
    > pip install -r requirements.txt
```

The file `notebooks/experiment.ipynb` can be used to run the code to obtain the results presented in the paper.

## Supplementary material

The scatter plots obtained with $`n={3,4,5,6,7,8}`$ are depicted below.

### Universal set with $`n=3`$ elements

![The scatter plots obtained by means of simulation with n=3.](notebooks/scatter_heat3.png)

### Universal set with $`n=4`$ elements

![The scatter plots obtained by means of simulation with n=4.](notebooks/scatter_heat4.png)

### Universal set with $`n=5`$ elements

![The scatter plots obtained by means of simulation with n=5.](notebooks/scatter_heat5.png)

### Universal set with $`n=6`$ elements

![The scatter plots obtained by means of simulation with n=6.](notebooks/scatter_heat6.png)

### Universal set with $`n=7`$ elements

![The scatter plots obtained by means of simulation with n=7.](notebooks/scatter_heat7.png)

### Universal set with $`n=8`$ elements

![The scatter plots obtained by means of simulation with n=8.](notebooks/scatter_heat8.png)

## Citation

Space for citation reference.

    @article{
        title={A comparative study of uncertainty measures in the Dempster-Shafer evidence theory}
        authors={...},
    }
